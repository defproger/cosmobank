<?php include "./php/head.php" ?>

<!-- cosmo screen start -->
<section class="cosmo-screen">
  <div class="space">
    <canvas id="canvas" width="100%" height="100vh">error, canvas not ready</canvas>
  </div>
  <div class="space-screen">
    <div class="container">
      <ul id="scene" class="scene">
        <li class="layer" data-depth="0.60"><img class="planet-img" src="./img/parralax-bg/1.png" alt=""></li>
        <li class="layer" data-depth="0.45"><img class="planet-img" src="./img/parralax-bg/2.png" alt=""></li>
        <li class="layer" data-depth="0.35"><img class="planet-img" src="./img/parralax-bg/3.png" alt=""></li>
        <li class="layer" data-depth="0.30"><img class="planet-img" src="./img/parralax-bg/4.png" alt=""></li>
        <li class="layer" data-depth="0.15"><img class="planet-img" src="./img/parralax-bg/5.png" alt=""></li>
        <li class="layer" data-depth="0.10"><img class="planet-img" src="./img/parralax-bg/6.png" alt=""></li>
      </ul>
    </div>
    <div class="texts">
      <div class="text-background">
        WE<span id="forbcosmo">B</span>ANK.
      </div>
      <div class="text-faceground">
        <p class="text">
          Банкинг на <br>
          космическом уровне!
        </p>
        <a class="btn" id="link4" href="#nextscreen">
          <svg class="liquid-button"
          data-text="GET STARTED"
          data-force-factor="0.1"
          data-layer-1-viscosity="0.5"
          data-layer-2-viscosity="0.4"
          data-layer-1-mouse-force="400"
          data-layer-2-mouse-force="500"
          data-layer-1-force-limit="1"
          data-layer-2-force-limit="2"
          data-color1="#ffdc41"
          data-color2="#5594fe"
          data-color3="#863be6"></svg>
        </a>
      </div>

    </div>
  </div>
</section>
<!-- cosmo screen end -->
<!-- card screen start -->
<section class="second_screen" id="cards">
	<div class="convert">

		<div class="left_mid">
			<div class="content_left">
				<div class="titles_left">
					<div class="title_item_big">one card</div>
					<div class="title_item_small">for all your payments.</div>
				</div>
				<div class="text_left">
          <p id="textcard">
            Карта премиум класса Maxi будет предоставлять доступ к функциональным возможностям сервиса с максимальным удобством и простотой использования.
          </p>
				</div>
				<a class="button_last" href="./php/reg.php">register card</a>
			</div>
		</div>

		<div class="right_mid">
			<div class="menu_right">
				<a id="maxi" class="menu_right_item"><p id="cardas">maxi-card</p></a>
				<a id="gold" class="menu_right_item"><p id="cardas">gold-card</p></a>
				<a id="iron" class="menu_right_item"><p id="cardas">iron-card</p></a>
			</div>
      <span>
			     <div class="rectangel_sc1"></div>
			     <div class="rectangel_sc"></div>
			     <div class="cards">
				         <img id="maxi" class="card_first maxi" src="img/cards/maxi.png" alt="">
				         <img id="gold" class="card_second gold" src="img/cards/gold.png" alt="">
				         <img id="iron" class="card_third iron" src="img/cards/iron.png" alt="">
			     </div>
      </span>
		</div>

	</div>
</section>
<!-- card screen end -->
<!-- kosmonavt screen start -->
<section class="kosmonavt" id="nextscreen">
  <div class="parralax-screen">
    <div class="planetaaa">
      <img id="planetaaa" src="./img/plannet1.png" alt="">
      <div class="linear-hr">
        <hr class="hrinkosmonavt hrk1">
        <hr class="hrinkosmonavt hrk2">
        <hr class="hrinkosmonavt hrk3">
      </div>
    </div>
    <div class="text-and-button">
      <h1 class="bigtextkosmonavt">Даже у него</h1>
      <h3 class="smalltextkosmonavt">Работает карта от COSMO BANK</h1>
      <div class="fbutton_">
        <a href="./php/reg.php" class="button_last_ ">register card</a>
      </div>

    </div>
    <div class="kosmonavt-schelovechek">
      <img class="astronaut_" src="./img/astronaut.png" alt="">
      <p class="astro-text">доступ в интернет</p>
      <p class="astro-text astro-text_">любое устройство</p>
    </div>
  </div>
</section>
<!-- kosmonavt screen end -->
<!-- beetweenscreens line start -->
<div class="beetweenscreens">
  <div>
    <div class="next-text-beetween">
      NEXT
    </div>
  </div>
</div>
<!-- beetweenscreens line end -->
<!-- foures screen start -->
<section class="screen_four" id="info">
		<div class="advantages">
			<div class="titles_ad">
				<div class="adv_title">ИНФОРМАЦИЯ</div>
				<div class="adv_subtitle">Как получить карту COSMOBANK</div>
			</div>
			<div class="blocks">

				<div class="standart1">
					<div class="rectangel-bl"></div>
					<div class="block">
						<div class="bl_title">Регистрация</div>
						<div class="mid_bl_text">
							<div class="bl_text">Вы регистрируетесь на нашем сайте, это можно сделать, нажав на кнопку <a id="linkess" href="./php/reg.php">register card</a></div>
							<img src="img/safe.png" alt="" class="bl_image">
						</div>
					</div>
					<div class="rectangel-bl-e"></div>
				</div>
				<div class="standart2">
					<div class="rectangel-bl2"></div>
					<div class="block">
						<div class="bl_title">Установка</div>
						<div class="mid_bl_text">
							<div class="bl_text">После регистрации вы можете скачать <a id="link15" href="#insstaler">фирменное приложение</a> с официального сайта </div>
						<img src="img/safe.png" alt="" class="bl_image">
					</div>
					</div>
					<div class="rectangel-bl-e2"></div>
				</div>

			</div>
			<div class="blocks">
				<div class="standart3">
					<div class="rectangel-bl3"></div>
					<div class="block">
						<div class="bl_title">Создание Карты</div>
						<div class="mid_bl_text">
							<div class="bl_text">Далее вы заходите в программу, либо в <a id="linkess" href="./php/log.php">панель пользователя</a> на сайте</div>
							<img src="img/safe.png" alt="" class="bl_image">
						</div>
					</div>
					<div class="rectangel-bl-e3"></div>
				</div>
				<div class="standart4">
					<div class="rectangel-bl4"></div>
					<div class="block ">
						<div class="bl_title">Заключение</div>
						<div class="mid_bl_text">
							<div class="bl_text">Выбираете тип карты, который вам нужен, ставите на неё ваш пароль и ГОТОВО, приятного использования</div>
							<img src="img/safe.png" alt="" class="bl_image">
						</div>
					</div>
					<div class="rectangel-bl-e4"></div>
				</div>

			</div>
			<div class="planets">
				<div class="planet1">
					<img src="img/21.png" alt="" class="planet">
				</div>
				<div class="planet2">
					<img src="img/21.png" alt="" class="planet">
				</div>
				<div class="planet3">
					<img src="img/21.png" alt="" class="planet">
				</div>
			</div>

		</div>
</section>
<!-- foures screen end -->
<!-- download line start -->
<section class="four_block" id="insstaler">
  <div class="info_block">
      <a class="convert_bl_info" href="./download/setupCosmo.exe" download="">
        <div class="conver_btn">download</div>
        <div class="conver_btn_gradient">app</div>
      </a>
    <div class="text_bl_info">Здесь вы можете скачать настольное приложение, в котором вы сможете зарегистрироваться либо войти в ваш аккаунт, для того чтобы проводить операции с картами, следить за балансом, осуществлять оплаты других сервисов.</div>
  </div>
</section>
<!-- download line end -->
<!-- prefooter start -->
<div id="dust">
</div>
<!-- prefooter end -->
<!-- random screen start / lightsreen-->
<section class="dastscreen" id="us">
  <div class="dastscreenblock">
    <div class="verybigtext">
      почему именно <span>мы!</span>
    </div>
    <div class="forhr">
      <hr id="hrnumber1">
      <hr id="hrnumber2">
    </div>
    <div class="cosmoimgcenter">
      <span class="rgbblok"></span>
      <span class="rgbblok rgbblok_"></span>
      <img class="for-z-zz" src="./img/spaceman.jpg" alt="">
      <div class="menusquare">
        <div class="firstsquare firstsquareh"></div>
        <div class="firstsquare lastsquare"></div>
      </div>
    </div>
    <div class="textes_">
      <div class="firstbigtext">
        <span id="texte">
          НАШИ ПРЕДЛОЖЕНИЯ
        </span>
      </div>
      <div class="textesesforlastmenu">
      <span id="text">
        <ul>
          <li>Скорость операций</li>
          <li>Защищенность от космической радиации</li>
          <li>Сопряженный с другими системами</li>
        </ul>
      </span>
      </div>
      <div class="lastbutton">
        <div class="fbutton_">
          <a href="./php/reg.php" class="button_last_ collores ">register card</a>
        </div>
      </div>
      <br>
      <br>
      <div class="neextnames">
        <p id="foranime"><span id="texte1">ЗАЩИТА ИНФОРМАЦИИ</span></p>
        <span class="lastspan">Нажмите сюда, чтобы посмотреть иную информацию</span>
      </div>
    </div>
  </div>
</section>
<!-- random screen end -->

<?php include "./php/footer.php" ?>
