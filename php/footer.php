  <!-- footer start -->
  <div class="csr">
    <section class="foot_screen">
    <footer>
        <div class="rectangel_left">
          <!-- MAKET -->
          <div class="menu">
            <a class="menu_it menu-item" id="link7" href="#info">Info</a>
            <a class="menu_it menu-item" id="link6" href="#us">about</a>
            <a class="menu_it menu-item mid_polosa" id="link5" href="#cards">cards</a>
            <div class="convertes convertes_x">
              <a id="bitch" class="headerbutton" href="./php/log.php" data-title="login"></a>
            </div>
          </div>

          <!-- MAKET -->
          <div class="obvertka_footer">

            <div class="footer_sub_title">
              cosmo
            </div>

            <div class="footer_title">
              we<span class="letter">b</span>ank
            </div>

          </div>
        </div>


      <div class="rectangel_right">
        <div class="right_info">
          <div class="scroll_footer">
            <a id="link10" class="scroll_item_one to-top">01</a>
            <a id="link11" href="#cards" class="scroll_item_two">02</a>
            <a id="link12" href="#nextscreen" class="scroll_item_three">03</a>
            <a id="link13" href="#info" class="scroll_item_four">04</a>
            <a id="link14" href="#us" class="scroll_item_fife">05</a>
            <a class="scroll_item_six">06</a>
          </div>
          <div class="footer_info">
            Наш проект направлен на демонстрацию принципов работы банкинга и обращения с деньгами онлайн. Вы можете управлять своим космо-кошельком с сайта или настольного приложения.
          </div>
          <div class="big_number">
            06
          </div>
        </div>
      </div>
    </footer>
  </section>
  </div>
  <!-- footer end -->
  <!-- js start -->
  <!-- simple js -->
  <script src="./js/headerbutton.js" charset="utf-8"></script>
  <!-- library js -->
  <script src="./js/three.min.js" charset="utf-8"></script>
  <script src="./js/orbitcontrols.js" charset="utf-8"></script>
  <script src="./js/chroma.min.js" charset="utf-8"></script>
  <script src="./js/dat.gui.min.js" charset="utf-8"></script>
  <script src="./js/particles.min.js" charset="utf-8"></script>
  <!-- code js -->
  <script src="./js/cosmotext.js" charset="utf-8"></script>
  <script src="./js/cosmoscreen.js" charset="utf-8"></script>
  <script src="./js/btn.js" charset="utf-8"></script>
  <!-- jquery js library-->
  <script src="./js/jquery.js" charset="utf-8"></script>
  <script src="./js/jquery.parallax.js"></script>
  <script src="./js/zepto.min.js"></script>
  <script src="./js/jquery.warpdrive.js" charset="utf-8"></script>
  <!-- jquery js code -->
  <script src="./js/link.js" charset="utf-8"></script>
  <script src="./js/parralax-screen.js" charset="utf-8"></script>
  <script>$('#scene').parallax();</script>
  <script src="./js/cardscreen.js" charset="utf-8"></script>
  <script src="./js/dust.js" charset="utf-8"></script>
  <script src="./js/lastscreen.js" charset="utf-8"></script>
  <script src="./js/adaptive.js" charset="utf-8"></script>
  <!-- <script src="./js/lightspeed.js" charset="utf-8"></script> -->



  <!-- js end -->

  </body>
</html>
