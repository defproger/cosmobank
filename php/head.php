<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>COSMO BANK</title>
	<!-- css -->
  <link rel="stylesheet" href="./css/reset.css">
	<link rel="stylesheet" href="./fonts/font.css">
  <link rel="stylesheet" href="./css/cosmo-screen.css">
	<link rel="stylesheet" href="./css/cardsscreen.css">
  <link rel="stylesheet" href="./css/kosmonavt.css">
	<link rel="stylesheet" href="./css/foures.css">
	<link rel="stylesheet" href="./css/dastscreen.css">
	<link rel="stylesheet" href="./css/footer.css">

	<link rel="shortcut icon" href="./favicon.ico">
</head>
<body>
<!-- menu start -->
<header>
  <div class="right-menu">
		<div id="textCOSMO">
			<!-- content generated with JS -->
		</div>
		<div id="textCosmoBank">BANK</div>
  </div>
	<div class="left-menu">
	  <nav class="navigation">
	    <a class="menu-item" id="link3" href="#info">Info</a>
	    <a class="menu-item" id="link2" href="#us">about</a>
	    <a class="menu-item" id="link" href="#cards">cards</a>
			<div class="convertes">
			  <a id="btnka" class="headerbutton" href="./php/log.php" data-title="login"></a>
			</div>

	  </nav>
	</div>
</header>
<!-- menu end -->
