
    $( document ).ready( function() {

        //------------------------------------------------------------------------

        //Settings - params for WarpDrive
        var settings = {

            // width: ,
            // height: ,
            autoResize: true,
            autoResizeMinWidth: null,
            autoResizeMaxWidth: null,
            autoResizeMinHeight: null,
            autoResizeMaxHeight: null,
            addMouseControls: true,
            addTouchControls: true,
            hideContextMenu: true,
            starCount: 1111,
            starBgCount: 999,
            starBgColor: { r:169, g:45, b:255 },
            starBgColorRangeMin: 1,
            starBgColorRangeMax: 40,
            starColor: { r:220, g:200, b:255 },
            starColorRangeMin: 1,
            starColorRangeMax: 90,
            starfieldBackgroundColor: { r:41, g:42, b:43 },
            starDirection: 1,
            starSpeed: 35,
            starSpeedMax: 95,
            starSpeedAnimationDuration: 5,
            starFov: 300,
            starFovMin: 200,
            starFovAnimationDuration: 2,
            starRotationPermission: true,
            starRotationDirection: 0,
            starRotationSpeed: 0.4,
            starRotationSpeedMax: 0.1,
            starRotationAnimationDuration: 2,
                starWarpLineLength: 1.0,
            starWarpTunnelDiameter: 100,
            starFollowMouseSensitivity: 0.025,
            starFollowMouseXAxis: true,
            starFollowMouseYAxis: true

        };

        //------------------------------------------------------------------------

        //standalone

        //init

        // var warpdrive = new WarpDrive( document.getElementById( 'holder' ) );
        // var warpdrive = new WarpDrive( document.getElementById( 'holder' ), settings );

        //------------------------------------------------------------------------

        //jQuery

        //init

        //$( '#holder' ).warpDrive();
        $( '#holder' ).warpDrive( settings );

        //------------------------------------------------------------------------

} );
