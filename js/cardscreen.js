$('#maxi').on('click', function() {

    $('#textcard').css({"opacity" : "0"});
    $('.maxi').css({"opacity" : "0"});
    $('.gold').css({"opacity" : "0"});
    $('.iron').css({"opacity" : "0"});
    setTimeout(function () {
      $('#textcard').remove();
      $('.text_left').prepend('<p id="textcard" style="opacity:0;">Карта премиум класса Maxi будет предоставлять доступ к функциональным возможностям сервиса с максимальным удобством и простотой использования.</p>')

      $('.cards').empty();
      $('.cards').prepend('<img id="iron" class="card_third iron" style="opacity:0;" src="img/cards/iron.png" alt="">')
                 .prepend('<img id="gold" class="card_second gold" style="opacity:0;" src="img/cards/gold.png" alt="">')
                 .prepend('<img id="maxi" class="card_first maxi" style="opacity:0;" src="img/cards/maxi.png" alt="">');

      setTimeout(function () {
        $('#textcard').css({"opacity" : "1"});
        $('.maxi').css({"opacity" : "1"});
        $('.gold').css({"opacity" : "1"});
        $('.iron').css({"opacity" : "1"});
      }, 150);
    }, 550);

});
$('#gold').on('click', function() {

      $('#textcard').css({"opacity" : "0"});
      $('.maxi').css({"opacity" : "0"});
      $('.gold').css({"opacity" : "0"});
      $('.iron').css({"opacity" : "0"});
      setTimeout(function () {
        $('#textcard').remove();
        $('.text_left').prepend('<p id="textcard" style="opacity:0;">Карта Gold - расширенные возможности использования данного функционала.</p>')

        $('.cards').empty();
        $('.cards').prepend('<img id="maxi" class="card_third maxi" style="opacity:0;" src="img/cards/maxi.png" alt="">')
                   .prepend('<img id="iron" class="card_second iron" style="opacity:0;" src="img/cards/iron.png" alt="">')
                   .prepend('<img id="gold" class="card_first gold" style="opacity:0;" src="img/cards/gold.png" alt="">');

        setTimeout(function () {
          $('#textcard').css({"opacity" : "1"});
          $('.maxi').css({"opacity" : "1"});
          $('.gold').css({"opacity" : "1"});
          $('.iron').css({"opacity" : "1"});
        }, 150);
      }, 550);

});
$('#iron').on('click', function() {
  $('#textcard').css({"opacity" : "0"});
  $('.maxi').css({"opacity" : "0"});
  $('.gold').css({"opacity" : "0"});
  $('.iron').css({"opacity" : "0"});
  setTimeout(function () {
    $('#textcard').remove();
    $('.text_left').prepend('<p id="textcard" style="opacity:0;">Карта Iron - максимальные возможности использования  сервиса с железными аргументами в его пользу.</p>')

    $('.cards').empty();
    $('.cards').prepend('<img id="gold" class="card_third gold" style="opacity:0;" src="img/cards/gold.png" alt="">')
               .prepend('<img id="maxi" class="card_second maxi" style="opacity:0;" src="img/cards/maxi.png" alt="">')
               .prepend('<img id="iron" class="card_first iron" style="opacity:0;" src="img/cards/iron.png" alt="">');

    setTimeout(function () {
      $('#textcard').css({"opacity" : "1"});
      $('.maxi').css({"opacity" : "1"});
      $('.gold').css({"opacity" : "1"});
      $('.iron').css({"opacity" : "1"});
    }, 150);
  }, 550);

});
